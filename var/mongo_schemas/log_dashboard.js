/**
 * @author surya_hanggara
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 14:22:43
 * @modify date 2021-02-18 14:25:16
 * @desc [description]
 */

exports.arrSchemaLog = {
    sources : {type: String, required: true},
    methode : {type: String, required: true},
    status : {type: Boolean, required: true},
    time_ex : {type: Date, required: true}
}
var express = require('express');
var router = express.Router();

var crypto = require('crypto')
var {select_for_login} = require('../lib/mysql/schema/sc_users')

const { connect } = require('../lib/mysql/bin/connection');

router.get('/test', (req, res) => {
  res.end("test")
})

/* GET users listing. 
  login authentication for user
*/
router.post('/get-acc', async (req, res) => {
  const nama = req.body['username']
  const password = req.body['password']

  let where = [nama, nama, nama, crypto.createHash('sha256').update(password).digest('hex')]
  var str_sql = select_for_login()

  var re_data = {status:false, message:"fail", data:{}}

  try {
    await connect(str_sql, where)
    .then(return_val => {
      re_data = {status:false, message:"fail", data:{}}
      if(return_val.status && return_val.data.length != 0){
        re_data.status = true
        re_data.message="success"
        re_data.data=return_val.data
      }
      res.json(re_data)
      res.end();
    }, error_val => {
      re_data = {status:false, message:error_val.message, data:{}}
      res.json(re_data)
      res.end();
    })  
  } catch (error) {
    re_data = {status:false, message:error.message, data:{}}
    res.json(re_data)
    res.end();
  }  
})

router.post('/insert_users', async (req, res) => {
  var id_appl_user = req.body['id_appl_user']
  var appl_m_unit_kerja_id = req.body['appl_m_unit_kerja_id']
  var appl_m_tipe_user_id = req.body['appl_m_tipe_user_id']
  var nama_user = req.body['nama_user']
  var kd_nm_user = req.body['kd_nm_user']
  var username = req.body['username']
  var nip = req.body['nip']
  var email = req.body['email']
  var no_hp = req.body['no_hp']
  var pass = req.body['pass']
  var auto_reset_pass = req.body['auto_reset_pass']
  var unit_kerja = req.body['unit_kerja']
  var wilayah = req.body['wilayah']
  var description = req.body['description']
  var disabled = req.body['disabled']
  var theme = req.body['theme']


  let where = [nama, nama, nama, crypto.createHash('sha256').update(password).digest('hex')]
  var str_sql = select_for_login()

  var re_data = {status:false, message:"fail", data:{}}

  try {
    await connect(str_sql, where)
    .then(return_val => {
      re_data = {status:false, message:"fail", data:{}}
      if(return_val.status && return_val.data.length != 0){
        re_data.status = true
        re_data.message="success"
        re_data.data=return_val.data
      }
      res.json(re_data)
      res.end();
    }, error_val => {
      re_data = {status:false, message:error_val.message, data:{}}
      res.json(re_data)
      res.end();
    })  
  } catch (error) {
    re_data = {status:false, message:error.message, data:{}}
    res.json(re_data)
    res.end();
  }  
})

module.exports = router;

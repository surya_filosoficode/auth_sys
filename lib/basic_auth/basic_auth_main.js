const { connect } = require('../mysql/bin/connection')

basic_auth = async () => {
    let where_basic = [username, password]
    var str_sql_basic = select_basic_auth(where_basic)

    return await connect(str_sql_basic.sql_statement, str_sql_basic.where)
    .then(return_val => {
        console.log(return_val)
        if(return_val.status && return_val.data.length != 0){
            next()
        }else{
            re_data = {status:false, message:"basic auth not authorized"}
            res.json(re_data)
            res.end()
        }
    }, error_val => {
        re_data = {status:false, message:error_val.message}
        res.json(re_data)
        res.end()
    })  
    
}

module.exports = {basic_auth}
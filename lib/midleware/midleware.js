const requestIp = require('request-ip')
const {select_all_basic} = require('../mysql/schema/md_ip_control')

const { connect } = require('../mysql/bin/connection')

// const { basic_auth } = require('../basic_auth/basic_auth_main')

basic_midleware = async (req, res, next) => {
    const clientIp = requestIp.getClientIp(req)

    if (!req.headers.authorization || req.headers.authorization.split(' ')[0] != "Basic") {
        res.status(401).json({ message: 'Missing Authorization Header' });
    }

    const base64Credentials =  req.headers.authorization.split(' ')[1];
    const credentials = Buffer.from(base64Credentials, 'base64').toString('ascii');
    const [username, password] = credentials.split(':');


    let where = [username, password, clientIp]
    var str_sql = select_all_basic(where)

    console.log(str_sql)

    try {
        await connect(str_sql.sql_statement, str_sql.where)
        .then(return_val => {
            if(return_val.status && return_val.data.length != 0){
                next()
            }else{
                re_data = {status:false, message:"midleware not authorized"}
                res.status(401).json(re_data)
                res.end()
            }
        }, error_val => {
            re_data = {status:false, message:error_val.message}
            res.status(401).json(re_data)
            res.end()
        })  
    } catch (error) {
        re_data = {status:false, message:error.message}
            res.status(401).json(re_data)
            res.end()
    }
}

module.exports = {basic_midleware}
/**
 * @author surya_hanggara
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 14:51:03
 * @modify date 2021-03-29 14:53:05
 * @desc [description]
 */

const mongoose = require('mongoose')

mongoSchema = (obj_schema) => {
    return mongoose.Schema(obj_schema)
}

mongoConnection = (mongoPath) => {
    return con_mongo = mongoose.createConnection(mongoPath, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
}

exports.mongoModel = (mongoModel, collection, obj_schema) => {
    return mongoConnection(mongoModel).model(collection, mongoSchema(obj_schema))
}
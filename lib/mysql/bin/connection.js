const mysql = require('mysql');
// const { promises } = require('node:fs');
const {basicConf, normalConf} = require('../config/db_conf')

let db = mysql.createConnection(normalConf("app_access")); //nama database

function connect (str_sql, where){
  return new Promise(resolve =>{
    const re_data = {status:false, message:"", data:{}}
    db.query(str_sql, where, (err, result) => {
        if (err) {
              re_data.message = err
              resolve(re_data)
        } else {
              re_data.status = true
              re_data.message = "success"
              re_data.data = result
              //   console.log(re_data)
              resolve(re_data)
        }
    })
  })
}


module.exports = {connect}
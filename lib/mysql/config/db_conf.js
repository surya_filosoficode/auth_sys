const mysql = require('mysql');

let basicConf = (req,res) => {
  return {connectionLimit: 10,
  host: "localhost",    // host
  user: "root",         // user
  password: "", // pass
  database: req}
}

let normalConf = (param) => {
  return {
  host: 'localhost',
  user: 'root',
  password: '',
  database: param}
}


module.exports = {basicConf, normalConf}
exports.select_ip = (where) => {
    return {
        "sql_statement":"SELECT * FROM appl_cf_ip WHERE ip_mechine = ?",
        "where":where,
    }
}

exports.select_basic_auth = (where) => {
    return {
        "sql_statement":"SELECT * FROM appl_cf_basic_auth WHERE username = ? and password = ?",
        "where":where,
    }
}


exports.select_all_basic = (where) => {
    return {
        "sql_statement":"SELECT * FROM appl_cf_basic_auth cb"+
                        " JOIN appl_cf_ip ci ON cb.id_appl_cf_basic_auth = ci.id_appl_cf_basic_auth"+
                        " WHERE cb.username = ? AND cb.password = ? AND ci.ip_mechine = ?",
        "where":where
    }
}
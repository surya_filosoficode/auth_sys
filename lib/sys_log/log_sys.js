/**
 * @author surya_hanggara
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:42:09
 * @modify date 2021-03-29 19:47:14
 * @desc [description]
 */

const date = require('../../var/date')

const mongoModel = require("../mongo/conn_mongo")
const mongoSchema = require("../../var/mongo_schemas/log_dashboard")

const path = require("../../var/mongo/path")
const coll = require("../../var/mongo/collections")

var mongoPath = path.main_path + path.path_log
var collection = coll.log_test

var arr_data = {
    "sources":"http://simpeg.malangkota.go.id/serv/get.php",
    "methode":"test",
    "status":true,
    "status_msg":"success",
    "time_ex":date.get_date_time()
}

var mongoModels = mongoModel.mongoModel(mongoPath, collection, mongoSchema.arrSchemaLog)

let msg = new mongoModels(arr_data)
msg.save().then(doc => {
    console.log(doc)
    console.log(doc.id)
}).catch(err => {
    console.log(err)
})
-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 09 Bulan Mei 2021 pada 16.40
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `app_access`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `appl_cf_basic_auth`
--

CREATE TABLE `appl_cf_basic_auth` (
  `id_appl_cf_basic_auth` varchar(32) NOT NULL,
  `app_name` varchar(64) DEFAULT NULL,
  `username` varchar(64) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `keteragan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `appl_cf_basic_auth`
--

INSERT INTO `appl_cf_basic_auth` (`id_appl_cf_basic_auth`, `app_name`, `username`, `password`, `keteragan`) VALUES
('CBA202104190000002', 'app_dashboard', 'lucu', 'lucu', 'testing');

--
-- Trigger `appl_cf_basic_auth`
--
DELIMITER $$
CREATE TRIGGER `ins_bf` BEFORE INSERT ON `appl_cf_basic_auth` FOR EACH ROW BEGIN
	DECLARE rowcount INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number WHERE id = "appl_cf_basic_auth";
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next 
    WHERE id = "appl_cf_basic_auth";
    
    SET fix_key_user = concat(kode_in, left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_appl_cf_basic_auth = fix_key_user;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `appl_cf_ip`
--

CREATE TABLE `appl_cf_ip` (
  `id_appl_cf_ip` varchar(32) NOT NULL,
  `id_appl_cf_basic_auth` varchar(32) DEFAULT NULL,
  `ip_mechine` varchar(16) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `status` enum('0','1') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `appl_cf_ip`
--

INSERT INTO `appl_cf_ip` (`id_appl_cf_ip`, `id_appl_cf_basic_auth`, `ip_mechine`, `keterangan`, `status`) VALUES
('CIP202104190000002', 'CBA202104190000002', '::1', 'localhost (test admin)', '1');

--
-- Trigger `appl_cf_ip`
--
DELIMITER $$
CREATE TRIGGER `ins_bf_ip_cf` BEFORE INSERT ON `appl_cf_ip` FOR EACH ROW BEGIN
	DECLARE rowcount INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number WHERE id = "appl_cf_ip";
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next 
    WHERE id = "appl_cf_ip";
    
    SET fix_key_user = concat(kode_in, left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_appl_cf_ip = fix_key_user;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `appl_h_group_item`
--

CREATE TABLE `appl_h_group_item` (
  `id_appl_h_group_item` varchar(32) NOT NULL,
  `appl_m_group_id` varchar(32) NOT NULL,
  `appl_menu_item_id` varchar(32) NOT NULL,
  `appl_m_modul_id` varchar(32) NOT NULL,
  `keterangan` text DEFAULT NULL,
  `created_by` varchar(20) NOT NULL DEFAULT '',
  `creation_date` datetime NOT NULL,
  `last_updated_by` varchar(20) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL,
  `hak_akses` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `appl_h_group_item`
--

INSERT INTO `appl_h_group_item` (`id_appl_h_group_item`, `appl_m_group_id`, `appl_menu_item_id`, `appl_m_modul_id`, `keterangan`, `created_by`, `creation_date`, `last_updated_by`, `last_updated_date`, `hak_akses`) VALUES
('HGI202011160000011', 'MGR202011020000001', 'MIT202011120000008', 'MMO202011020000011', 'aa', '', '0000-00-00 00:00:00', NULL, NULL, NULL),
('HGI202011170000012', 'MGR202011020000001', 'MIT202011110000005', 'MMO202011020000012', 'awd', '', '0000-00-00 00:00:00', NULL, NULL, 'rw'),
('HGI202011170000013', 'MGR202011020000001', 'MIT202011120000006', 'MMO202011020000012', 'aad', '', '0000-00-00 00:00:00', NULL, NULL, 'r'),
('HGI202011170000014', 'MGR202011020000001', 'MIT202011120000007', 'MMO202011020000012', 'awd', '', '0000-00-00 00:00:00', NULL, NULL, 'na');

--
-- Trigger `appl_h_group_item`
--
DELIMITER $$
CREATE TRIGGER `ins_appl_h_group_item` BEFORE INSERT ON `appl_h_group_item` FOR EACH ROW BEGIN
	DECLARE rowcount INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number WHERE id = "appl_h_group_item";
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next 
    WHERE id = "appl_h_group_item";
    
    SET fix_key_user = concat(kode_in, left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_appl_h_group_item = fix_key_user;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `appl_h_modul_menu`
--

CREATE TABLE `appl_h_modul_menu` (
  `id_appl_h_modul_menu` varchar(32) NOT NULL,
  `appl_m_modul_id` varchar(32) NOT NULL,
  `appl_menu_item_id` varchar(32) NOT NULL,
  `keterangan` text DEFAULT NULL,
  `created_by` varchar(20) NOT NULL DEFAULT '',
  `creation_date` datetime NOT NULL,
  `last_updated_by` varchar(20) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `appl_h_modul_menu`
--

INSERT INTO `appl_h_modul_menu` (`id_appl_h_modul_menu`, `appl_m_modul_id`, `appl_menu_item_id`, `keterangan`, `created_by`, `creation_date`, `last_updated_by`, `last_updated_date`) VALUES
('HMM202011130000007', 'MMO202011020000002', 'MIT202011120000007', NULL, '', '0000-00-00 00:00:00', NULL, NULL),
('HMM202011130000012', 'MMO202011020000002', 'MIT202011120000010', NULL, '', '0000-00-00 00:00:00', NULL, NULL),
('HMM202011130000013', 'MMO202011020000002', 'MIT202011120000011', NULL, '', '0000-00-00 00:00:00', NULL, NULL);

--
-- Trigger `appl_h_modul_menu`
--
DELIMITER $$
CREATE TRIGGER `ins_appl_h_modul_menu` BEFORE INSERT ON `appl_h_modul_menu` FOR EACH ROW BEGIN
	DECLARE rowcount INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number WHERE id = "appl_h_modul_menu";
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next 
    WHERE id = "appl_h_modul_menu";
    
    SET fix_key_user = concat(kode_in, left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_appl_h_modul_menu = fix_key_user;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `appl_h_user_group`
--

CREATE TABLE `appl_h_user_group` (
  `id_appl_h_user_group` varchar(32) NOT NULL,
  `appl_user_id` varchar(32) NOT NULL,
  `appl_m_group_id` varchar(32) NOT NULL,
  `keterangan` text DEFAULT NULL,
  `created_by` varchar(20) NOT NULL DEFAULT '',
  `creation_date` datetime NOT NULL,
  `last_updated_by` varchar(20) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Trigger `appl_h_user_group`
--
DELIMITER $$
CREATE TRIGGER `ins_appl_h_user_group` BEFORE INSERT ON `appl_h_user_group` FOR EACH ROW BEGIN
	DECLARE rowcount INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number WHERE id = "appl_h_user_group";
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next 
    WHERE id = "appl_h_user_group";
    
    SET fix_key_user = concat(kode_in, left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_appl_h_user_group = fix_key_user;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `appl_menu_item`
--

CREATE TABLE `appl_menu_item` (
  `id_appl_menu_item` varchar(32) NOT NULL,
  `appl_menu_item_id` varchar(32) NOT NULL,
  `appl_m_modul_id` varchar(32) NOT NULL,
  `pattern_parent` text DEFAULT NULL,
  `seq` int(11) NOT NULL,
  `prompt` varchar(30) NOT NULL,
  `url` varchar(200) DEFAULT NULL,
  `class_name` varchar(50) DEFAULT NULL,
  `document` varchar(200) DEFAULT NULL,
  `parameter` varchar(100) DEFAULT NULL,
  `submenu` int(11) DEFAULT NULL,
  `target` varchar(25) DEFAULT NULL,
  `created_by` varchar(20) NOT NULL DEFAULT '',
  `creation_date` datetime NOT NULL,
  `last_updated_by` varchar(20) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `appl_menu_item`
--

INSERT INTO `appl_menu_item` (`id_appl_menu_item`, `appl_menu_item_id`, `appl_m_modul_id`, `pattern_parent`, `seq`, `prompt`, `url`, `class_name`, `document`, `parameter`, `submenu`, `target`, `created_by`, `creation_date`, `last_updated_by`, `last_updated_date`) VALUES
('MIT202011110000005', '0', 'MMO202011020000012', '[]', 0, 'yu', 'awd', 'awd', 'awd', 'awd', 0, '0', '', '0000-00-00 00:00:00', NULL, NULL),
('MIT202011120000006', '0', 'MMO202011020000012', '[]', 0, 'nadms', 'awda', 'awd', 'awd', 'awd', 0, '0', '', '0000-00-00 00:00:00', NULL, NULL),
('MIT202011120000007', 'MIT202011110000005', 'MMO202011020000012', '[\"MIT202011110000005\"]', 0, 'aa', 'awdawd', 'awd', 'awd', 'awd', 1, '0', '', '0000-00-00 00:00:00', NULL, NULL),
('MIT202011120000008', 'MIT202011110000005', 'MMO202011020000011', '[\"MIT202011110000005\"]', 0, 'bb', 'awdawdawda', 'adw', 'awd', 'dawdaw', 1, '0', '', '0000-00-00 00:00:00', NULL, NULL),
('MIT202011120000009', 'MIT202011120000007', 'MMO202011020000012', '[\"MIT202011110000005\",\"MIT202011120000007\"]', 0, 'a1', 'aef', 'wef', 'wef', 'wef', 2, '0', '', '0000-00-00 00:00:00', NULL, NULL),
('MIT202011120000010', 'MIT202011120000007', 'MMO202011020000012', '[\"MIT202011110000005\",\"MIT202011120000007\"]', 0, 'a1 duaz', 'awdwadaz', 'awdz', 'awdz', 'awdz', 2, '0', '', '0000-00-00 00:00:00', NULL, NULL),
('MIT202011120000011', 'MIT202011120000009', 'MMO202011020000012', '[\"MIT202011110000005\",\"MIT202011120000007\",\"MIT202011120000009\"]', 0, 'a2 ', 'qq', 'awd', 'awd', 'awd', 3, '0', '', '0000-00-00 00:00:00', NULL, NULL),
('MIT202011120000012', 'MIT202011120000009', 'MMO202011020000012', '[\"MIT202011110000005\",\"MIT202011120000007\",\"MIT202011120000009\"]', 0, 'a2 awd', 'awdawdawdaa', 'af', 'aefeaf', 'aef', 3, '0', '', '0000-00-00 00:00:00', NULL, NULL),
('MIT202011120000013', 'MIT202011120000011', 'MMO202011020000012', '[\"MIT202011110000005\",\"MIT202011120000007\",\"MIT202011120000009\",\"MIT202011120000011\"]', 0, 'a3', 'aefaeszz', 'awd', 'aef', 'sef', 4, '0', '', '0000-00-00 00:00:00', NULL, NULL),
('MIT202011120000014', 'MIT202011120000011', 'MMO202011020000012', '[\"MIT202011110000005\",\"MIT202011120000007\",\"MIT202011120000009\",\"MIT202011120000011\"]', 0, 'a3 tytuiu', 'eeee', 'awd', 'awd', 'awd', 4, '0', '', '0000-00-00 00:00:00', NULL, NULL),
('MIT202011120000015', 'MIT202011120000013', 'MMO202011020000012', '[\"MIT202011110000005\",\"MIT202011120000007\",\"MIT202011120000009\",\"MIT202011120000011\",\"MIT202011120000013\"]', 0, 'a4', 'wqew', 'awdw', 'awd', 'awd', 5, '0', '', '0000-00-00 00:00:00', NULL, NULL),
('MIT202011120000016', 'MIT202011120000013', 'MMO202011020000012', '[\"MIT202011110000005\",\"MIT202011120000007\",\"MIT202011120000009\",\"MIT202011120000011\",\"MIT202011120000013\"]', 0, 'a4 ghjkl', 'ttr', 'awd', 'awdaw', 'awdaw', 5, '0', '', '0000-00-00 00:00:00', NULL, NULL),
('MIT202011120000017', 'MIT202011120000013', 'MMO202011020000012', '[\"MIT202011110000005\",\"MIT202011120000007\",\"MIT202011120000009\",\"MIT202011120000011\",\"MIT202011120000013\"]', 0, 'a4 ghjkl', 'ttrewrt', 'awd', 'awdaw', 'awdaw', 5, '0', '', '0000-00-00 00:00:00', NULL, NULL),
('MIT202011120000018', 'MIT202011120000007', 'MMO202011020000012', '[\"MIT202011110000005\",\"MIT202011120000007\"]', 0, 'a1 tigaz', 'zz', 'efz', 'wefz', 'wefz', 2, '0', '', '0000-00-00 00:00:00', NULL, NULL),
('MIT202011120000019', 'MIT202011110000005', 'MMO202011020000012', '[\"MIT202011110000005\"]', 0, 'cc', 'dwefgrthyjuia', 'awd', 'awd', 'awdwd', 1, '0', '', '0000-00-00 00:00:00', NULL, NULL),
('MIT202011120000020', '0', 'MMO202011020000012', '[]', 0, 'ptiga', 'rr', 'awd', 'awd', 'awd', 0, '0', '', '0000-00-00 00:00:00', NULL, NULL),
('MIT202011120000021', 'MIT202011120000020', 'MMO202011020000012', '[\"MIT202011120000020\"]', 0, 'b1', 'awdas', 'awd', 'awd', 'awd', 1, '0', '', '0000-00-00 00:00:00', NULL, NULL),
('MIT202011120000023', 'MIT202011120000021', 'MMO202011020000012', '[\"MIT202011120000020\",\"MIT202011120000021\"]', 0, 'c1', 'qwer', 'daw', 'awd', 'awd', 2, '0', '', '0000-00-00 00:00:00', NULL, NULL),
('MIT202011120000024', 'MIT202011120000021', 'MMO202011020000012', '[\"MIT202011120000020\",\"MIT202011120000021\"]', 0, 'c1 we', 'ww', 'awd', 'awd', 'awd', 2, '0', '', '0000-00-00 00:00:00', NULL, NULL);

--
-- Trigger `appl_menu_item`
--
DELIMITER $$
CREATE TRIGGER `ins_item_menu` BEFORE INSERT ON `appl_menu_item` FOR EACH ROW BEGIN
	DECLARE rowcount INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number WHERE id = "appl_menu_item";
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next 
    WHERE id = "appl_menu_item";
    
    SET fix_key_user = concat(kode_in, left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_appl_menu_item = fix_key_user;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `appl_my_app`
--

CREATE TABLE `appl_my_app` (
  `id_appl_my_app` varchar(32) NOT NULL,
  `id_appl_user` varchar(32) DEFAULT NULL,
  `nm_app` text DEFAULT NULL,
  `keterangan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `appl_my_app`
--

INSERT INTO `appl_my_app` (`id_appl_my_app`, `id_appl_user`, `nm_app`, `keterangan`) VALUES
('APP202104190000003', 'USR202011100000001', 'dashboard', 'dashboard');

--
-- Trigger `appl_my_app`
--
DELIMITER $$
CREATE TRIGGER `bf_ins` BEFORE INSERT ON `appl_my_app` FOR EACH ROW BEGIN
	DECLARE rowcount INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number WHERE id = "appl_my_app";
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next 
    WHERE id = "appl_my_app";
    
    SET fix_key_user = concat(kode_in, left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_appl_my_app = fix_key_user;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `appl_m_group`
--

CREATE TABLE `appl_m_group` (
  `id_appl_m_group` varchar(32) NOT NULL,
  `nm_group` varchar(255) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `created_by` varchar(32) NOT NULL DEFAULT '',
  `creation_date` datetime NOT NULL,
  `last_updated_by` varchar(32) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `appl_m_group`
--

INSERT INTO `appl_m_group` (`id_appl_m_group`, `nm_group`, `description`, `created_by`, `creation_date`, `last_updated_by`, `last_updated_date`) VALUES
('MGR202011020000001', NULL, NULL, '', '0000-00-00 00:00:00', NULL, NULL);

--
-- Trigger `appl_m_group`
--
DELIMITER $$
CREATE TRIGGER `ins_appl_m_group` BEFORE INSERT ON `appl_m_group` FOR EACH ROW BEGIN
	DECLARE rowcount INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number WHERE id = "appl_m_group";
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next 
    WHERE id = "appl_m_group";
    
    SET fix_key_user = concat(kode_in, left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_appl_m_group = fix_key_user;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `appl_m_modul`
--

CREATE TABLE `appl_m_modul` (
  `id_appl_m_modul` varchar(32) NOT NULL,
  `kd_modul` varchar(20) NOT NULL,
  `modul_name` varchar(20) NOT NULL,
  `modul_path` varchar(50) DEFAULT NULL,
  `db_driver` varchar(30) NOT NULL,
  `db_host` varchar(30) NOT NULL,
  `db_user` varchar(30) NOT NULL,
  `db_pswd` varchar(50) DEFAULT NULL,
  `db_name` varchar(30) NOT NULL,
  `db_options` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `created_by` varchar(32) NOT NULL DEFAULT '',
  `creation_date` datetime NOT NULL,
  `last_updated_by` varchar(32) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `appl_m_modul`
--

INSERT INTO `appl_m_modul` (`id_appl_m_modul`, `kd_modul`, `modul_name`, `modul_path`, `db_driver`, `db_host`, `db_user`, `db_pswd`, `db_name`, `db_options`, `description`, `created_by`, `creation_date`, `last_updated_by`, `last_updated_date`) VALUES
('MMO202011020000001', 'APPL', 'Administrator', 'admin', 'mysql', 'localhost', 'root', 'password', 'lived_sikd_2020_new', '', '', '', '0000-00-00 00:00:00', 'agustinus', '2012-01-16 21:15:53'),
('MMO202011020000002', 'PRNC', 'Planning Budgeting', 'modul/sikd_2020', 'mysql', 'localhost', 'root', 'password', 'sikd_bwi_2020_new', '', '', 'admin', '2008-04-20 09:14:02', 'darmawan', '2019-11-13 21:16:40'),
('MMO202011020000003', 'SETUP', 'Setup SIKD', 'modul/sikd_2020/setup', 'mysql', 'localhost', 'root', 'password', 'sikd_bwi_2020_new', '', '', 'admin', '2008-04-24 10:44:18', 'sumber', '2012-01-16 00:27:50'),
('MMO202011020000004', 'PNTS', 'Penatausahaan', 'modul/sikd_2020/penatausahaan', 'mysql', 'localhost', 'root', 'password', 'sikd_bwi_2020_new', '', '', 'admin', '2008-12-01 07:56:06', 'agustinus', '2012-01-15 23:28:18'),
('MMO202011020000005', 'EKSK', 'Eksekutif', 'modul/sikd_2020/eksekutif', 'mysql', 'localhost', 'root', 'password', 'sikd_bwi_2020_new', '', '', 'admin', '2008-12-01 07:58:29', 'admin', '2011-01-28 08:01:48'),
('MMO202011020000006', 'AKUN', 'Akuntansi', 'modul/sikd_2020/akun', 'mysql', 'localhost', 'root', 'password', 'sikd_bwi_2020_new', '', '', 'admin', '2009-01-27 10:38:44', 'admin', '2011-01-28 08:01:28'),
('MMO202011020000007', 'PRBN', 'Perubahan', 'modul/sikd_2020/dpa_p', 'mysql', 'localhost', 'root', 'password', 'sikd_bwi_2020_new', '', '', 'admin', '2009-05-13 12:01:03', 'admin', '2011-01-28 08:03:19'),
('MMO202011020000008', 'RPJMD', 'RPJMD', 'modul/sikd_2020/lakip', 'mysql', 'localhost', 'root', 'password', 'sikd_bwi_2020_new', '', '', 'admin', '2009-11-03 10:37:51', 'sumber', '2016-09-09 09:11:29'),
('MMO202011020000009', 'SPD', 'Simpada', 'modul/sikd_2020/simpada', 'mysql', 'localhost', 'root', 'password', 'simpada', '', '', 'sumber', '2011-12-12 23:16:17', 'sumber', '2011-12-12 23:21:43'),
('MMO202011020000010', 'SPDP', 'Simpada NON SKP', 'modul/sikd_2020/penatausahaan', 'mysql', 'localhost', 'root', 'password', 'sikd_bwi_2020_new', '', '', 'sumber', '2014-09-22 21:15:13', 'sumber', '2014-11-26 19:18:49'),
('MMO202011020000011', 'GDR', 'Gender', 'modul/sikd_2020/gender', 'mysql', 'localhost', 'root', 'password', 'sikd_bwi_2020_new', '', '', 'sumber', '2016-11-28 11:09:42', '', '0000-00-00 00:00:00'),
('MMO202011020000012', 'SIRUP', 'Sirup', 'modul/sikd_2020/sirup', 'mysql', 'localhost', 'root', 'password', 'sikd_bwi_2020_new', '', '', 'sumber', '2017-01-08 23:32:25', '', '0000-00-00 00:00:00'),
('MMO202011020000013', 'ASB', 'ASB', 'modul/sikd_2020/setup', 'mysql', 'localhost', 'root', 'password', 'sikd_bwi_2020_new', '', '', 'sumber', '2018-08-16 11:27:06', 'sumber', '2018-08-20 10:26:07');

--
-- Trigger `appl_m_modul`
--
DELIMITER $$
CREATE TRIGGER `ins_appl_m_modul` BEFORE INSERT ON `appl_m_modul` FOR EACH ROW BEGIN
	DECLARE rowcount INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number WHERE id = "appl_m_modul";
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next 
    WHERE id = "appl_m_modul";
    
    SET fix_key_user = concat(kode_in, left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_appl_m_modul = fix_key_user;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `appl_m_tipe_user`
--

CREATE TABLE `appl_m_tipe_user` (
  `id_appl_m_tipe_user` varchar(32) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `priviledge` varchar(5) NOT NULL,
  `auth_class` varchar(255) NOT NULL,
  `created_by` varchar(32) NOT NULL DEFAULT '',
  `creation_date` datetime NOT NULL,
  `last_updated_by` varchar(32) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `appl_m_tipe_user`
--

INSERT INTO `appl_m_tipe_user` (`id_appl_m_tipe_user`, `keterangan`, `priviledge`, `auth_class`, `created_by`, `creation_date`, `last_updated_by`, `last_updated_date`) VALUES
('MTU202011020000001', '', '', '', '', '0000-00-00 00:00:00', NULL, NULL);

--
-- Trigger `appl_m_tipe_user`
--
DELIMITER $$
CREATE TRIGGER `id_appl_m_tipe_user` BEFORE INSERT ON `appl_m_tipe_user` FOR EACH ROW BEGIN
	DECLARE rowcount INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number WHERE id = "appl_m_tipe_user";
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next 
    WHERE id = "appl_m_tipe_user";
    
    SET fix_key_user = concat(kode_in, left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_appl_m_tipe_user = fix_key_user;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `appl_m_unit_kerja`
--

CREATE TABLE `appl_m_unit_kerja` (
  `id_appl_m_unit_kerja` varchar(32) NOT NULL,
  `nm_unit_kerja` varchar(120) NOT NULL,
  `singkatan` varchar(30) DEFAULT NULL,
  `id_induk` varchar(10) DEFAULT NULL,
  `created_by` varchar(32) NOT NULL DEFAULT '',
  `creation_date` datetime NOT NULL,
  `last_updated_by` varchar(32) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `appl_m_unit_kerja`
--

INSERT INTO `appl_m_unit_kerja` (`id_appl_m_unit_kerja`, `nm_unit_kerja`, `singkatan`, `id_induk`, `created_by`, `creation_date`, `last_updated_by`, `last_updated_date`) VALUES
('MUK202011020000001', '', NULL, NULL, '', '0000-00-00 00:00:00', NULL, NULL);

--
-- Trigger `appl_m_unit_kerja`
--
DELIMITER $$
CREATE TRIGGER `id_appl_m_unit_kerja` BEFORE INSERT ON `appl_m_unit_kerja` FOR EACH ROW BEGIN
	DECLARE rowcount INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number WHERE id = "appl_m_unit_kerja";
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next 
    WHERE id = "appl_m_unit_kerja";
    
    SET fix_key_user = concat(kode_in, left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_appl_m_unit_kerja = fix_key_user;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `appl_user`
--

CREATE TABLE `appl_user` (
  `id_appl_user` varchar(32) NOT NULL,
  `appl_m_unit_kerja_id` varchar(32) NOT NULL,
  `appl_m_tipe_user_id` varchar(32) DEFAULT NULL,
  `nama_user` varchar(40) DEFAULT NULL,
  `kd_nm_user` varchar(255) DEFAULT NULL,
  `username` varchar(32) DEFAULT NULL,
  `nip` varchar(18) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `no_hp` varchar(50) DEFAULT NULL,
  `pass` varchar(255) NOT NULL,
  `auto_reset_pass` int(11) DEFAULT 0,
  `unit_kerja` varchar(20) DEFAULT NULL,
  `wilayah` varchar(10) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `disabled` char(1) DEFAULT '0',
  `theme` varchar(30) NOT NULL,
  `created_by` varchar(20) NOT NULL DEFAULT '',
  `creation_date` datetime NOT NULL,
  `last_updated_by` varchar(20) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `appl_user`
--

INSERT INTO `appl_user` (`id_appl_user`, `appl_m_unit_kerja_id`, `appl_m_tipe_user_id`, `nama_user`, `kd_nm_user`, `username`, `nip`, `email`, `no_hp`, `pass`, `auto_reset_pass`, `unit_kerja`, `wilayah`, `description`, `disabled`, `theme`, `created_by`, `creation_date`, `last_updated_by`, `last_updated_date`) VALUES
('USR202011100000001', 'MUK202011020000001', 'MTU202011020000001', 'user dinkes', 'g', 'pakwali', '12345', 'g', 'g', '8C6976E5B5410415BDE908BD4DEE15DFB167A9C873FC4BB8A81F6F2AB448A918', 0, 'g', 'g', 'g', 'g', 'g', 'g', '0000-00-00 00:00:00', 'g', '0000-00-00 00:00:00'),
('USR202011100000002', 'MUK202011020000002', 'MTU202011030000002', 'user kominfo', 'g', 'surya', '56789', 'g', 'g', '8C6976E5B5410415BDE908BD4DEE15DFB167A9C873FC4BB8A81F6F2AB448A918', 0, 'g', 'g', 'g', 'g', 'g', 'g', '0000-00-00 00:00:00', 'g', '0000-00-00 00:00:00');

--
-- Trigger `appl_user`
--
DELIMITER $$
CREATE TRIGGER `ins_appl_user` BEFORE INSERT ON `appl_user` FOR EACH ROW BEGIN
	DECLARE rowcount INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number WHERE id = "appl_user";
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next 
    WHERE id = "appl_user";
    
    SET fix_key_user = concat(kode_in, left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_appl_user = fix_key_user;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `index_number`
--

CREATE TABLE `index_number` (
  `id` varchar(32) NOT NULL,
  `id_index` varchar(100) NOT NULL,
  `kode` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `index_number`
--

INSERT INTO `index_number` (`id`, `id_index`, `kode`) VALUES
('appl_cf_basic_auth', '2', 'CBA'),
('appl_cf_ip', '2', 'CIP'),
('appl_h_modul_menu', '1', 'HMM'),
('appl_menu_item', '1', 'MIT'),
('appl_my_app', '3', 'APP'),
('appl_m_group', '1', 'MGR'),
('appl_m_modul', '13', 'MMO'),
('appl_m_tipe_user', '1', 'MTU'),
('appl_m_unit_kerja', '1', 'MUK'),
('appl_user', '1', 'USR');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `appl_cf_basic_auth`
--
ALTER TABLE `appl_cf_basic_auth`
  ADD PRIMARY KEY (`id_appl_cf_basic_auth`);

--
-- Indeks untuk tabel `appl_cf_ip`
--
ALTER TABLE `appl_cf_ip`
  ADD PRIMARY KEY (`id_appl_cf_ip`) USING BTREE;

--
-- Indeks untuk tabel `appl_h_group_item`
--
ALTER TABLE `appl_h_group_item`
  ADD PRIMARY KEY (`id_appl_h_group_item`) USING BTREE;

--
-- Indeks untuk tabel `appl_h_modul_menu`
--
ALTER TABLE `appl_h_modul_menu`
  ADD PRIMARY KEY (`id_appl_h_modul_menu`) USING BTREE;

--
-- Indeks untuk tabel `appl_h_user_group`
--
ALTER TABLE `appl_h_user_group`
  ADD PRIMARY KEY (`id_appl_h_user_group`) USING BTREE;

--
-- Indeks untuk tabel `appl_menu_item`
--
ALTER TABLE `appl_menu_item`
  ADD PRIMARY KEY (`id_appl_menu_item`) USING BTREE;

--
-- Indeks untuk tabel `appl_my_app`
--
ALTER TABLE `appl_my_app`
  ADD PRIMARY KEY (`id_appl_my_app`);

--
-- Indeks untuk tabel `appl_m_group`
--
ALTER TABLE `appl_m_group`
  ADD PRIMARY KEY (`id_appl_m_group`) USING BTREE;

--
-- Indeks untuk tabel `appl_m_modul`
--
ALTER TABLE `appl_m_modul`
  ADD PRIMARY KEY (`id_appl_m_modul`) USING BTREE,
  ADD UNIQUE KEY `kode_modul` (`kd_modul`) USING BTREE;

--
-- Indeks untuk tabel `appl_m_tipe_user`
--
ALTER TABLE `appl_m_tipe_user`
  ADD PRIMARY KEY (`id_appl_m_tipe_user`) USING BTREE;

--
-- Indeks untuk tabel `appl_m_unit_kerja`
--
ALTER TABLE `appl_m_unit_kerja`
  ADD PRIMARY KEY (`id_appl_m_unit_kerja`) USING BTREE;

--
-- Indeks untuk tabel `appl_user`
--
ALTER TABLE `appl_user`
  ADD PRIMARY KEY (`id_appl_user`) USING BTREE;

--
-- Indeks untuk tabel `index_number`
--
ALTER TABLE `index_number`
  ADD PRIMARY KEY (`id`,`kode`) USING BTREE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
